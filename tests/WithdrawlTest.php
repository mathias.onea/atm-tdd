<?php


namespace Test;


use App\Withdrawal;
use App\AtmGateway;
use PHPUnit\Framework\TestCase;

class WithdrawalTest extends TestCase
{
    /** @test */
    public function itCanWithdrawASingleInstanceOfABill(): void
    {
        $this->assertEquals([50], Withdrawal::factory(new AtmGateway)->withdraw(50));
    }

    /** @test */
    public function itCanWithdrawMultipleInstancesOfABill(): void
    {
        $this->assertEquals([100,50], Withdrawal::factory(new AtmGateway)->withdraw(150));
        $this->assertEquals([500,500], Withdrawal::factory(new AtmGateway)->withdraw(1000));
    }
}
