<?php


namespace App;


class Withdrawal
{
    /**
     * @var AtmGateway
     */
    private $atmGateway;

    public function __construct(AtmGateway $atmGateway)
    {
        $this->atmGateway = $atmGateway;
    }

    public static function factory(AtmGateway $atmGateway)
    {
        return new self($atmGateway);
    }

    public function withdraw(int $requested)
    {

        $result = [];
        $remainingAmount = $requested;
        foreach($this->atmGateway->availability() as $availableBill => $availableBillAmount){
            if ($availableBill > $remainingAmount) {
                continue;
            }

            foreach(range($availableBillAmount,1,-1) as $remainingBillAmount){
                $remainingAmount -= $availableBill;
                $result[] = $availableBill;

                if ($remainingAmount === 0) {
                    return $result;
                }
            }

        }

        return $result;
    }
}
