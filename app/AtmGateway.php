<?php


namespace App;


class AtmGateway
{
    public function availability()
    {
        return [
            500 => 5,
            200 => 2,
            100 => 1,
            50  => 2,
            20  => 10,
            10  => 20,
            5   => 50
        ];
    }
}
